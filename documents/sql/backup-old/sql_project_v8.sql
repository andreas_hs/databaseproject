-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11. Mai, 2022 14:01 PM
-- Tjener-versjon: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sql_project`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `authorization`
--

CREATE TABLE `authorization` (
  `username` varchar(50) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `hashedpassword` varchar(64) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `salt` varchar(16) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `role` varchar(50) COLLATE utf8mb4_danish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `authorization`
--

INSERT INTO `authorization` (`username`, `hashedpassword`, `salt`, `role`) VALUES
('user1', '51c05203c326e1698960b96ef6ed542ec98890026ffa902bce0a0ff1ed51d33d', 'ZfRac6rqqLunYu9x', 'Storekeeper'),
('user2', '418ac11089ee307e9d82800306bc6ec08eadcb57c7dc1dee8222732689aa898d', 'duqLaFtDWvZkbBhk', 'Customer'),
('user3', '5a6a4c7447e977e6e0c6077448864681ee79752e209187a395722a43e0e39ae0', 'Ib4jYiznuGkDdiZ6', 'Customer Rep'),
('user4', 'cee1c6f1c018212e7c7f065795cecf8188a862658c737e58a5d93f5583281dff', 'iigsXHFhA4PPAE9s', 'Production planner'),
('user5', '8ccbf0c5d37320522db916037410391f0d4923ca91015d32d7204f1113a4a6bf', 'GAHv472M4PNHF2lp', 'Transporter');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `startDate` date DEFAULT NULL,
  `endDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `customer`
--

INSERT INTO `customer` (`id`, `name`, `startDate`, `endDate`) VALUES
(1, 'xxl', '2012-07-20', '2017-09-21'),
(2, 'sport1', '2030-03-22', '2026-06-22'),
(3, 'sveinungs jallashop', '2001-04-22', '2011-11-22');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `customerrep`
--

CREATE TABLE `customerrep` (
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `customerrep`
--

INSERT INTO `customerrep` (`employee_id`) VALUES
(3);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `driver`
--

CREATE TABLE `driver` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `transporter_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `driver`
--

INSERT INTO `driver` (`id`, `name`, `transporter_id`) VALUES
(1, 'ole', 1),
(2, 'dole', 1),
(3, 'doffen', 2);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `department` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `employee`
--

INSERT INTO `employee` (`id`, `name`, `department`, `manufacturer_id`) VALUES
(1, 'mike tyson', 'sales', 1),
(2, 'sveinung olsen', 'sales', 1),
(3, 'ola nordmann', 'production', 2),
(4, 'will smith', 'distribution', 2);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `franchise`
--

CREATE TABLE `franchise` (
  `id` int(11) NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `buyingPrice` float NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `franchisestore`
--

CREATE TABLE `franchisestore` (
  `id` int(11) NOT NULL,
  `address` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `buyFLag` tinyint(1) DEFAULT NULL,
  `franchise_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `manufacturer`
--

CREATE TABLE `manufacturer` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `manufacturer`
--

INSERT INTO `manufacturer` (`id`, `name`) VALUES
(1, 'madshus'),
(2, 'fisher');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `product`
--

CREATE TABLE `product` (
  `model` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `description` varchar(50) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `historical` tinyint(1) DEFAULT NULL,
  `url` varchar(100) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `msrp` float DEFAULT NULL,
  `type` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `manufacturer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `product`
--

INSERT INTO `product` (`model`, `description`, `historical`, `url`, `msrp`, `type`, `manufacturer_id`) VALUES
('fastAFski', 'jdsgomw', 0, '123spill.com', 4477, 'classic', 2),
('milla plank', 'oinrvoanevoea', 0, '123spill.com', 6536, 'classic', 2),
('skate', 'reeeee', 0, 'http.google.com', 5378, 'trash', 1);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `productionplan`
--

CREATE TABLE `productionplan` (
  `id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `product_model` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `productionplan`
--

INSERT INTO `productionplan` (`id`, `employee_id`, `product_model`, `quantity`, `startDate`, `endDate`) VALUES
(1, 1, 'fastAFski', 10, '2022-05-09', '2022-06-06');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `productionplanner`
--

CREATE TABLE `productionplanner` (
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `productionplanner`
--

INSERT INTO `productionplanner` (`employee_id`) VALUES
(4);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `productorder`
--

CREATE TABLE `productorder` (
  `id` int(11) NOT NULL,
  `totalPrice` int(11) DEFAULT NULL,
  `state` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `customer_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `productorder`
--

INSERT INTO `productorder` (`id`, `totalPrice`, `state`, `customer_id`) VALUES
(1, 7647457, 'ready to be shipped', 1),
(2, 573657, 'skis available', 2),
(3, 1231, 'open', 3),
(4, 0, 'recieved', 1),
(5, 0, 'recieved', 1),
(6, 0, 'recieved', 1),
(7, 0, 'recieved', 1),
(8, 0, 'recieved', 1);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `productsinorder`
--

CREATE TABLE `productsinorder` (
  `product_model` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `productorder_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `size_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `productsinorder`
--

INSERT INTO `productsinorder` (`product_model`, `productorder_id`, `quantity`, `size_id`) VALUES
('fastAFski', 4, 10, 142),
('fastAFski', 5, 10, 147),
('fastAFski', 6, 10, 192),
('skate', 6, 10, 182),
('fastAFski', 8, 10, 142),
('skate', 8, 5, 162);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `shipment`
--

CREATE TABLE `shipment` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci DEFAULT NULL,
  `address` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `pickupDate` date NOT NULL,
  `transporter_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `productorder_id` int(11) NOT NULL,
  `status` varchar(50) COLLATE utf8mb4_danish_ci DEFAULT 'ready to be shipped'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `shipment`
--

INSERT INTO `shipment` (`id`, `name`, `address`, `pickupDate`, `transporter_id`, `driver_id`, `manufacturer_id`, `productorder_id`, `status`) VALUES
(1, 'reeeeee', 'retardgata 1', '2011-11-11', 1, 1, 1, 1, 'ready to be shipped'),
(2, 'vsvfsdfv', 'veiengata 234', '2012-01-22', 2, 2, 2, 3, 'ready to be shipped'),
(3, 'oireada', 'gate vegen 12', '2012-12-12', 1, 1, 1, 3, 'ready to be shipped'),
(4, 'XXL', '', '2022-05-12', 1, 2, 2, 4, 'ready to be shipped'),
(5, 'XXL', 'test', '2022-05-10', 1, 1, 1, 1, 'ready to be shipped');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `size`
--

CREATE TABLE `size` (
  `size` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `size`
--

INSERT INTO `size` (`size`) VALUES
(142),
(147),
(152),
(157),
(162),
(167),
(172),
(177),
(182),
(187),
(192),
(197),
(202),
(207);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `stock`
--

CREATE TABLE `stock` (
  `product_model` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `size_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `store`
--

CREATE TABLE `store` (
  `address` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `buyingPrice` float NOT NULL,
  `customer_id` int(11) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `store`
--

INSERT INTO `store` (`address`, `buyingPrice`, `customer_id`, `id`) VALUES
('retardgata 1', 4325, 3, 1),
('kongensgate 12', 1412, 1, 2),
('teknologivegen 22', 123, 2, 3);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `storekeeper`
--

CREATE TABLE `storekeeper` (
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `storekeeper`
--

INSERT INTO `storekeeper` (`employee_id`) VALUES
(1),
(2);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `teamskier`
--

CREATE TABLE `teamskier` (
  `dob` date NOT NULL,
  `club` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL,
  `newSkiesPerYear` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `transporter`
--

CREATE TABLE `transporter` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8mb4_danish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_danish_ci;

--
-- Dataark for tabell `transporter`
--

INSERT INTO `transporter` (`id`, `name`) VALUES
(1, 'ola'),
(2, 'ole');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customerrep`
--
ALTER TABLE `customerrep`
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `driver`
--
ALTER TABLE `driver`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transporter_id` (`transporter_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`);

--
-- Indexes for table `franchise`
--
ALTER TABLE `franchise`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `franchisestore`
--
ALTER TABLE `franchisestore`
  ADD PRIMARY KEY (`id`),
  ADD KEY `franchise_id` (`franchise_id`);

--
-- Indexes for table `manufacturer`
--
ALTER TABLE `manufacturer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`model`),
  ADD KEY `manufacturer_id` (`manufacturer_id`);

--
-- Indexes for table `productionplan`
--
ALTER TABLE `productionplan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `employee_id` (`employee_id`),
  ADD KEY `product_model` (`product_model`);

--
-- Indexes for table `productionplanner`
--
ALTER TABLE `productionplanner`
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `productorder`
--
ALTER TABLE `productorder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `productsinorder`
--
ALTER TABLE `productsinorder`
  ADD KEY `size_id` (`size_id`),
  ADD KEY `product_model` (`product_model`),
  ADD KEY `productorder_id` (`productorder_id`);

--
-- Indexes for table `shipment`
--
ALTER TABLE `shipment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transporter_id` (`transporter_id`),
  ADD KEY `driver_id` (`driver_id`),
  ADD KEY `manufacturer_id` (`manufacturer_id`),
  ADD KEY `productorder_id` (`productorder_id`);

--
-- Indexes for table `size`
--
ALTER TABLE `size`
  ADD PRIMARY KEY (`size`);

--
-- Indexes for table `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`product_model`,`size_id`),
  ADD KEY `size_id` (`size_id`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `storekeeper`
--
ALTER TABLE `storekeeper`
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `teamskier`
--
ALTER TABLE `teamskier`
  ADD PRIMARY KEY (`id`),
  ADD KEY `customer_id` (`customer_id`);

--
-- Indexes for table `transporter`
--
ALTER TABLE `transporter`
  ADD PRIMARY KEY (`id`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `customerrep`
--
ALTER TABLE `customerrep`
  ADD CONSTRAINT `customerrep_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);

--
-- Begrensninger for tabell `driver`
--
ALTER TABLE `driver`
  ADD CONSTRAINT `driver_ibfk_1` FOREIGN KEY (`transporter_id`) REFERENCES `transporter` (`id`);

--
-- Begrensninger for tabell `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`);

--
-- Begrensninger for tabell `franchise`
--
ALTER TABLE `franchise`
  ADD CONSTRAINT `franchise_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Begrensninger for tabell `franchisestore`
--
ALTER TABLE `franchisestore`
  ADD CONSTRAINT `franchisestore_ibfk_1` FOREIGN KEY (`franchise_id`) REFERENCES `franchise` (`id`);

--
-- Begrensninger for tabell `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`);

--
-- Begrensninger for tabell `productionplan`
--
ALTER TABLE `productionplan`
  ADD CONSTRAINT `productionplan_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `productionplan_ibfk_2` FOREIGN KEY (`product_model`) REFERENCES `product` (`model`);

--
-- Begrensninger for tabell `productionplanner`
--
ALTER TABLE `productionplanner`
  ADD CONSTRAINT `productionplanner_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);

--
-- Begrensninger for tabell `productorder`
--
ALTER TABLE `productorder`
  ADD CONSTRAINT `productorder_ibfk_2` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Begrensninger for tabell `productsinorder`
--
ALTER TABLE `productsinorder`
  ADD CONSTRAINT `productsinorder_ibfk_1` FOREIGN KEY (`size_id`) REFERENCES `size` (`size`),
  ADD CONSTRAINT `productsinorder_ibfk_2` FOREIGN KEY (`product_model`) REFERENCES `product` (`model`),
  ADD CONSTRAINT `productsinorder_ibfk_3` FOREIGN KEY (`productorder_id`) REFERENCES `productorder` (`id`);

--
-- Begrensninger for tabell `shipment`
--
ALTER TABLE `shipment`
  ADD CONSTRAINT `shipment_ibfk_1` FOREIGN KEY (`transporter_id`) REFERENCES `transporter` (`id`),
  ADD CONSTRAINT `shipment_ibfk_2` FOREIGN KEY (`driver_id`) REFERENCES `driver` (`id`),
  ADD CONSTRAINT `shipment_ibfk_3` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturer` (`id`),
  ADD CONSTRAINT `shipment_ibfk_4` FOREIGN KEY (`productorder_id`) REFERENCES `productorder` (`id`);

--
-- Begrensninger for tabell `stock`
--
ALTER TABLE `stock`
  ADD CONSTRAINT `stock_ibfk_1` FOREIGN KEY (`product_model`) REFERENCES `product` (`model`),
  ADD CONSTRAINT `stock_ibfk_2` FOREIGN KEY (`size_id`) REFERENCES `size` (`size`);

--
-- Begrensninger for tabell `store`
--
ALTER TABLE `store`
  ADD CONSTRAINT `store_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);

--
-- Begrensninger for tabell `storekeeper`
--
ALTER TABLE `storekeeper`
  ADD CONSTRAINT `storekeeper_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`);

--
-- Begrensninger for tabell `teamskier`
--
ALTER TABLE `teamskier`
  ADD CONSTRAINT `teamskier_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
