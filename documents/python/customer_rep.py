from flask import request, jsonify
from flask_mysqldb import MySQL
import init

app = init.init()
mysql = MySQL(app)


@app.route('/customer_rep/retrieve_order', methods=['GET'])
def retrieve_orders():
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        response = cur.execute("SELECT * FROM `productorder` WHERE `state`= 'skis available';")

        if response == 1:
            skis_available = cur.fetchone()
        elif response > 1:
            skis_available = cur.fetchall()

        cur.close()
        return jsonify(skis_available), 200  # Return every order with skis available state and 200 OK status code
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/customer_rep/change_state', methods=['POST'])
def change_order():
    if request.method == 'POST':
        data = request.get_json()
        id = data['id']  # -> id to change
        state = data['state']  # -> state to change to

        cur = mysql.connection.cursor()
        cur.execute("UPDATE `productorder` SET `state`=%s WHERE `id`=%s;", (state, id,))
        mysql.connection.commit()
        response = cur.execute("SELECT * FROM `productorder` WHERE `id`=%s;", (id,))
        # Here there's always just one response
        if response > 0:
            order_data = cur.fetchone()
        cur.close()

        return jsonify(order_data), 201  # returns the data changed with 201 CREATED status code
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/customer_rep/create_shipment', methods=['POST'])
def create_shipment():
    if request.method == 'POST':
        data = request.get_json()
        product_order_id = data['product_order_id']
        name = data['name']
        address = data['address']
        pickup_date = data['pickup_date']
        transporter_id = data['transporter_id']
        driver_id = data['driver_id']
        cur = mysql.connection.cursor()
        get_orders_ready = cur.execute("SELECT * FROM `productorder` WHERE `state`='ready to be shipped' AND `id`=%s",
                                       (product_order_id,))
        if get_orders_ready > 0:
            orders_ready = cur.fetchone()
        else:
            return "Could not find any order ready to be shipped with given id.", 400
        manufacturer_id = orders_ready[3]
        get_shipment_id = cur.execute("SELECT id FROM shipment ORDER BY id DESC")
        if get_shipment_id > 0:
            shipment = cur.fetchone()
            new_shipment_id = str(shipment[0] + 1)
        else:
            new_shipment_id = 1

        create_new_shipment = cur.execute(
            "INSERT INTO `shipment`(`id`, `name`, `address`, `pickupDate`, `transporter_id`, `driver_id`, `manufacturer_id`, `productorder_id`, `status`)"
            " VALUES (%s, %s, %s, %s, %s, %s, %s, %s, 'ready to be shipped')",
            (new_shipment_id, name, address, pickup_date, transporter_id, driver_id, manufacturer_id, product_order_id,))
        mysql.connection.commit()

        get_new_shipment = cur.execute("SELECT * FROM `shipment` WHERE `id`=%s", (new_shipment_id,))
        if get_new_shipment > 0:
            new_shipment = cur.fetchone()
        else:
            return "Could not get created shipment.", 500

        return jsonify(new_shipment), 200
    else:
        return "Unsupported method, only POST for this endpoint.", 501
