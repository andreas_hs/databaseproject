from flask import request, jsonify
from flask_mysqldb import MySQL
import init

app = init.init()
mysql = MySQL(app)


@app.route('/transporter/retrieve_info', methods=['GET'])
def retrieve_info():
    # SELECT * FROM order WHERE status = ready to be shipped
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        response = cur.execute("SELECT * FROM productorder WHERE state = 'ready to be shipped'")

        if response == 1:
            order_info = cur.fetchone()
        elif response > 1:
            order_info = cur.fetchall()
        else:
            return "no orders ready to be shipped", 200

        cur.close()
        return jsonify(order_info), 200  # Return every order with skis available state and 200 OK status code
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/transporter/change_state', methods=['POST'])
def change_shipment_state():
    # need to add status to shipment
    data = request.get_json()
    shipment_id = int(data['shipment_id'])
    cur = mysql.connection.cursor()
    cur.execute("UPDATE shipment SET status = 'picked up' WHERE id=%s", (shipment_id,))
    return "status updated", 200
