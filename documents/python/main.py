from flask import session
from flask_mysqldb import MySQL

import customer_rep
import storekeeper
import transporter
import public
import login
import customer
import production_planner
import init

app = init.init()
mysql = MySQL(app)


# NB: use cur.fetchall() for multiple responses and cur.fetchone() for one single response

@app.route('/')
def index():
    return "Server online"


# Public endpoint
@app.route('/get_ski_types', methods=['GET'])
def get_ski_types():
    return public.get_ski_types()


# customerrep endpoint

@app.route('/customer_rep/retrieve_order', methods=['GET'])
def customer_rep_retrieve_orders():
    if session['role'] == "Customer Rep":
        return customer_rep.retrieve_orders()
    else:
        return "Not authorized for this endpoint.", 403


# Add a check for input (this endpoint is currently working as the 2 and 3 endpoint for customer rep)
@app.route('/customer_rep/change_state', methods=['POST'])
def customer_rep_change_state():
    if session['role'] == "Customer Rep":
        return customer_rep.change_order()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/customer_rep/create_shipment', methods=['POST'])
def customer_rep_create_shipment():
    if session['role'] == "Customer Rep":
        return customer_rep.create_shipment()
    else:
        return "Not authorized for this endpoint.", 403


# storekeeper endpoint

@app.route('/storekeeper/retrieve_orders', methods=['GET'])
def storekeeper_retrieve_orders():
    if session['role'] == "Storekeeper":
        return storekeeper.retrieve_orders()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/storekeeper/fill_order', methods=['POST'])
def storekeeper_fill_order():
    if session['role'] == "Storekeeper":
        return storekeeper.fill_order()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/storekeeper/update_stock', methods=['POST'])
def update_stock():
    if session['role'] == "Storekeeper":
        return storekeeper.update_stock()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/Storekeeper/new_product', methods=['POST'])
def new_product():
    return storekeeper.new_product()


# shipper endpoint

@app.route('/transporter/retrieve_info', methods=['GET'])
def transporter_retrieve_info():
    if session['role'] == "Transporter":
        return transporter.retrieve_info()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/transporter/change_state', methods=['POST'])
def transporter_change_shipment_state():
    if session['role'] == "Transporter":
        return transporter.change_shipment_state()
    else:
        return "Not authorized for this endpoint.", 403


# Customer endpoint

@app.route('/customer/create_order_one_product', methods=['POST'])
def customer_create_order():
    if session['role'] == "Customer":
        return customer.create_order_one_product()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/customer/create_order_multiple_products', methods=['POST'])
def create_order_multiple_products():
    if session['role'] == "Customer":
        return customer.create_order_multiple_products()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/customer/cancel_order', methods=['POST'])
def customer_cancel_order():
    if session['role'] == "Customer":
        return customer.cancel_order()
    else:
        return "Not authorized for this endpoint.", 403


@app.route('/customer/retrieve_production_plan', methods=['GET'])
def customer_retrieve_production_plan():
    if session['role'] == "Customer":
        return customer.retrieve_production_plan()
    else:
        return "Not authorized for this endpoint.", 403


# Production planner endpoint


@app.route('/production_planner/create_new_production_plan', methods=['POST'])
def production_planner_create_new_production_plan():
    if session['role'] == "Production planner":
        return production_planner.upload_production_plan()
    else:
        return "Not authorized for this endpoint.", 403


# Login endpoint

@app.route('/login', methods=['POST', 'GET'])
def login_main():
    return login.login_function()


@app.route('/logout', methods=['GET'])
def logout():
    return login.logout()


if __name__ == '__main__':
    app.run(debug=True)
