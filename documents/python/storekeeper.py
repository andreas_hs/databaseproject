from flask import request, jsonify
from flask_mysqldb import MySQL
import init

app = init.init()
mysql = MySQL(app)


@app.route('/storekeeper/retrieve_orders', methods=['GET'])
def retrieve_orders():
    if request.method == 'GET':
        cur = mysql.connection.cursor()
        response = cur.execute("SELECT * FROM `productorder` WHERE `state`= 'skis available';")

        if response == 1:
            skis_available = cur.fetchone()
        elif response > 1:
            skis_available = cur.fetchall()

        cur.close()
        return jsonify(skis_available), 200  # Return every order with skis available state and 200 OK status code
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/storekeeper/fill_order', methods=['POST'])
def fill_order():
    if request.method == 'POST':
        data = request.get_json()
        id = data['id']  # -> id of order to be ready to be shipped

        cur = mysql.connection.cursor()
        change_state_of_order = cur.execute(
            "UPDATE `productorder` SET `state`='ready to be shipped' WHERE `id`=%s", (id,))
        mysql.connection.commit()
        response = cur.execute("SELECT * FROM `productorder` WHERE `id`=%s", (id,))

        if response > 0:
            order_filled = cur.fetchone()

        cur.close()
        return jsonify(order_filled), 200
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/storekeeper/update_stock', methods=['POST'])
def update_stock():
    cur = mysql.connection.cursor()
    if request.method == 'POST':
        data = request.get_json()
        model_name = data['model_name']
        ski_type = data['ski_type']
        size = data['size']
        quantity = data['quantity']

        cur.execute("SELECT model FROM `product`")
        product_list = cur.fetchall()

        list2 = []
        for model_name1 in product_list:
            list2.append(model_name1[0])
        # if new model:
        if not (model_name in list2):
            manufacturer_id = data['manufacturer_id']
            update_existing_product = cur.execute(
                " INSERT INTO product(model, `type`, manufacturer_id)"
                " VALUES (%s, %s, %s)",
                (model_name, ski_type, manufacturer_id,))
            mysql.connection.commit()

            update_existing_stock = cur.execute(
                "INSERT INTO stock(`size_id`, product_model, quantity)"
                " VALUES (%s, %s, %s)",
                (size, model_name, quantity,))
            mysql.connection.commit()

            return "new entry created", 200
        else:
            stock_list = cur.execute("SELECT product_model FROM `stock`")
            stock_list = cur.fetchall()
            list2 = []
            for modelname1 in stock_list:
                list2.append(modelname1[0])

            if model_name in list2:
                given_product_model = data['model_name']
                size = data['size']
                quantity = data['quantity']
                old_quantity = cur.execute("SELECT quantity FROM stock WHERE product_model = %s AND `size_id`= %s ",
                                           (given_product_model, size,))
                new_quantity = old_quantity + quantity

                update_existing_stock = cur.execute(
                    "UPDATE stock SET quantity=%s WHERE product_model=%s",
                    (new_quantity, given_product_model,))
                mysql.connection.commit()
            else:
                update_notexisting_stock = cur.execute(
                    "INSERT INTO stock(`size_id`, product_model, quantity)"
                    "VALUES (%s, %s, %s)",
                    (size, model_name, quantity,))
                if update_notexisting_stock == 0:
                    return "bad request", 400
                mysql.connection.commit()
            return "entry updated", 200
    else:
        return "Unsupported method, only GET for this endpoint.", 501


@app.route('/storekeeper/new_product', methods=['POST'])
def new_product():
    if request.method == 'POST':
        data = request.get_json()
        model = data['model']
        type = data['type']
        description = data['description']
        historical = data['historical']
        url = data['url']
        msrp = data['msrp']
        manufacturer_id = data['manufacturer_id']

        cur = mysql.connection.cursor()

        # TODO: CHECK IF PRIMARY KEY ALREADY EXISTS

        update_existing_product = cur.execute(
            " INSERT INTO `product`(`model`, `description`, `historical`, `url`, `msrp`, `type`, `manufacturer_id`)"
            " VALUES (%s, %s, %s, %s, %s, %s, %s)",
            (model, description, historical, url, float(msrp), type, manufacturer_id,))
        mysql.connection.commit()

        orders_info = cur.execute("SELECT * FROM `product` ORDER BY `model` DESC")

        if orders_info > 0:
            orders_info = cur.fetchone()
        else:
            return "Something went wrong, please try again", 500

        cur.close()
        return jsonify(orders_info), 201
    else:
        return "Unsupported method, only GET for this endpoint.", 501
