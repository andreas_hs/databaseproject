from flask import request, jsonify
from flask_mysqldb import MySQL
import init

app = init.init()
mysql = MySQL(app)


@app.route('/customer/create_order_one_product', methods=['POST'])
def create_order_one_product():
    if request.method == "POST":
        data = request.get_json()
        model = data['model']
        quantity = data['quantity']
        customer_id = data['customer_id']
        size = data['size']

        cur = mysql.connection.cursor()
        is_model_valid = cur.execute("SELECT * FROM `product` WHERE model=%s", (model,))
        if is_model_valid == 0:
            return "Not a valid model name.", 400
        # product = cur.fetchone()  # -> only gets one row from db
        # manufacturer_id = str(product[len(product) - 1])

        is_size_valid = cur.execute("SELECT * FROM `size` WHERE `size`=%s", (size,))
        if is_size_valid == 0:
            return "Not a valid size.", 400

        get_product_order = cur.execute("SELECT id FROM productorder ORDER BY id DESC;")
        if get_product_order == 0:
            new_order_id = 1
        else:
            product_order = cur.fetchone()
            new_order_id = str(product_order[0] + 1)

        create_new_order = cur.execute("INSERT INTO `productorder`(`id`, `totalPrice`, `state`, `customer_id`)"
                                       " VALUES (%s, 0,'recieved', %s)",
                                       (new_order_id, customer_id,))
        mysql.connection.commit()

        create_products_in_order = cur.execute(
            "INSERT INTO `productsinorder`(`product_model`, `productorder_id`, `quantity`, `size_id`)"
            " VALUES (%s, %s, %s, %s)",
            (model, new_order_id, quantity, size,))
        mysql.connection.commit()

        get_new_order = cur.execute("SELECT * FROM `productorder` WHERE `id`=%s", (new_order_id,))
        new_order = cur.fetchone()
        cur.close()

        return jsonify(new_order), 201
    else:
        return "Method not supported", 405


@app.route('/customer/create_order_multiple_products', methods=['POST'])
def create_order_multiple_products():
    if request.method == "POST":
        data = request.get_json()
        customer_id = data['customer_id']
        list_of_products = data['products']

        cur = mysql.connection.cursor()
        get_product_order = cur.execute("SELECT id FROM productorder ORDER BY id DESC;")
        if get_product_order == 0:
            new_order_id = 1
        else:
            product_order = cur.fetchone()
            new_order_id = str(product_order[0] + 1)

        create_new_order = cur.execute("INSERT INTO `productorder`(`id`, `totalPrice`, `state`, `customer_id`)"
                                       " VALUES (%s, 0,'recieved', %s)",
                                       (new_order_id, customer_id,))
        mysql.connection.commit()

        for products in range(len(list_of_products)):
            model = list_of_products[products][0]
            is_model_valid = cur.execute("SELECT * FROM `product` WHERE model=%s", (model,))
            if is_model_valid == 0:
                return "Not a valid model name.", 400

            quantity = list_of_products[products][1]
            if quantity < 0:
                return "Not a valid quantity number.", 400

            size = list_of_products[products][2]
            is_size_valid = cur.execute("SELECT * FROM `size` WHERE `size`=%s", (size,))
            if is_size_valid == 0:
                return "Not a valid size.", 400

            create_products_in_order = cur.execute(
                "INSERT INTO `productsinorder`(`product_model`, `productorder_id`, `quantity`, `size_id`)"
                " VALUES (%s, %s, %s, %s)",
                (model, new_order_id, quantity, size,))
            mysql.connection.commit()
        get_new_order = cur.execute("SELECT * FROM `productsinorder` WHERE `productorder_id`=%s", (new_order_id,))
        new_order = cur.fetchall()
        return jsonify(new_order), 200
    else:
        return "Method not supported", 405


@app.route('/customer/cancel_order', methods=['POST'])
def cancel_order():
    if request.method == "POST":
        data = request.get_json()
        customer_id = data['customer_id']
        order_id = data['order_id']
        cur = mysql.connection.cursor()
        delete_order = cur.execute("UPDATE `productorder` SET state='cancel' WHERE id=%s AND customer_id=%s", (order_id, customer_id,))
        mysql.connection.commit()
        if delete_order == 0:
            return "Could not cancel order.", 400
        return "Order canceled", 202
    else:
        return "Method not supported", 405


@app.route('/customer/retrieve_production_plan', methods=['GET'])
def retrieve_production_plan():
    if request.method == "GET":
        cur = mysql.connection.cursor()
        get_production_plans = cur.execute("SELECT `product_model`, `quantity` FROM `productionplan` WHERE 1")
        if get_production_plans > 0:
            production_plans = cur.fetchall()
        else:
            return "Could not find any production plans.", 500
        return jsonify(production_plans), 200
    else:
        return "Method not supported", 405
