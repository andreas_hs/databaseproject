from flask import request
from flask_mysqldb import MySQL
from datetime import datetime, timedelta
import init

app = init.init()
mysql = MySQL(app)


@app.route('/production_planner/create_new_production_plan', methods=['POST'])
def upload_production_plan():
    if request.method == "POST":
        data = request.get_json()
        employee_id = data['employee_id']
        product_model = data['product_model']
        quantity = data['quantity']
        start_date = datetime.now()  # -> need to find today's date.
        td = timedelta(days=28)
        end_date = start_date + td  # -> start_date + 4 weeks
        production_plan_id = 0

        cur = mysql.connection.cursor()
        get_production_plan = cur.execute("SELECT id FROM `productionplan` ORDER BY id DESC;")
        if get_production_plan == 0:
            production_plan_id = 1
        else:
            get_production_plan_id = cur.fetchone()
            production_plan_id = str(get_production_plan_id[0] + 1)

        create_new_plan = cur.execute("INSERT INTO `productionplan`(`id`,`employee_id`, `product_model`, `quantity`, `startDate`, `endDate`)"
                                      " VALUES (%s, %s, %s, %s, %s, %s)",
                                      (production_plan_id, employee_id, product_model, quantity, start_date, end_date,))
        mysql.connection.commit()
        cur.close()
        return "New production plan created.", 201
    else:
        return "Unsupported method, only POST for this endpoint.", 501
