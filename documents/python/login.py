from flask import request, session
from flask_mysqldb import MySQL
from hashlib import pbkdf2_hmac
import random
import init

app = init.init()
mysql = MySQL(app)


@app.route('/logout', methods=['GET'])
def logout():
    if session['role'] == "":
        return "Already logged out.", 400
    else:
        session['role'] = ""
        return "logged out.", 200


@app.route('/login', methods=['POST'])
def login_function():
    if request.method == 'POST':
        data = request.get_json()
        user_name = data['username']
        password = data['password']
        cur = mysql.connection.cursor()
        response = cur.execute("SELECT * FROM `authorization` WHERE username = %s", (user_name,))
        # Here we just get one response
        if response > 0:
            user_data = cur.fetchone()  # -> only gets one row from db
            cur.close()
        else:
            return "Invalid username.", 500

        db_password = user_data[1]  # The hashed-password stored in the db
        db_salt = user_data[2]  # The random salt stored in the db
        db_role = user_data[3]  # The role of this user trying to log in

        if hash_password(password, db_salt).hex() == db_password:
            session['role'] = db_role
            session['username'] = user_name
            return "Approved as role : " + db_role, 200
        else:
            return "Not approved", 403  # -> mismatch between stored hashed-password and the newly created.
    else:
        return "Unsupported method, only GET for this endpoint.", 501


# Used to create the random salt used in the db
def random_salt_maker():
    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    chars = []
    for i in range(16):
        chars.append(random.choice(alphabet))
    return "".join(chars)  # -> returns a random salt of length 16


# Used to create hashed password to check up against hashed password in db
def hash_password(password, salt):
    our_app_iters = 500_000
    password_as_byte = str.encode(password)
    salt_as_byte = str.encode(salt)
    return pbkdf2_hmac('sha256', password_as_byte, salt_as_byte, our_app_iters)
