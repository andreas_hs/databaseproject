from flask import request, jsonify
from flask_mysqldb import MySQL
import init

app = init.init()
mysql = MySQL(app)


@app.route('/get_ski_types', methods=['GET'])
def get_ski_types():
    if request.method == 'GET':
        cur = mysql.connection.cursor()

        model = request.args.get("model", "")
        size = request.args.get("size", 0)
        if len(model) > 0 and size == 0:
            print(model)
            response = cur.execute("SELECT * FROM `product` WHERE model=%s", (model,))  # -> one filter
        elif len(model) > 0 and int(size) > 0:
            size_response = cur.execute("SELECT * FROM `size` WHERE `size`=%s", (size,))
            if size_response == 0:
                return "Not a valid size.", 400

            response = cur.execute(
                "SELECT model,description,historical,url,msrp,type,manufacturer_id,stock.size_id FROM product INNER JOIN `stock` ON product_model=product.model WHERE `product_model`=%s AND `size_id`=%s",
                (model, int(size),))
            # -> above is if two filter
        else:
            response = cur.execute("SELECT * FROM `product`")  # -> no filter

        if response > 0:
            response = cur.fetchall()
        else:
            return "Could not find valid response. Try again with other size or model values.", 400

        cur.close()
        return jsonify(response), 200  # -> return the data found to user with 200 OK status code
    else:
        return "Unsupported method, only GET for this endpoint.", 501
