import json
import unittest
import main
from flask import Flask, session, request

import unittest

test_client = main.app.test_client()


class MyTestCase(unittest.TestCase):

    # testing the index page

    def test_index_page_get(self):
        response = test_client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode('UTF-8'), "Server online")

    # testing public endpoint

    def test_get_skis(self):
        response = test_client.get('/get_ski_types')
        self.assertEqual(response.status_code, 200)

    # testing login endpoint

    def test_login_feature_get(self):
        response = test_client.get('/login')
        self.assertEqual(response.status_code, 501)
        self.assertEqual(response.data.decode(), "Unsupported method, only GET for this endpoint.")

    def test_login_feature_no_body(self):
        response = test_client.post('/login')
        self.assertEqual(response.status_code, 400)

    def test_login_feature_storekeeper(self):
        body = {
            "username": "user1",
            "password": "password1"
        }
        response = test_client.post('/login', json=body)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Approved as role : Storekeeper")

    def test_login_feature_customer(self):
        body = {
            "username": "user2",
            "password": "password2"
        }
        response = test_client.post('/login', json=body)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Approved as role : Customer")

    def test_login_feature_customer_rep(self):
        body = {
            "username": "user3",
            "password": "password3"
        }
        response = test_client.post('/login', json=body)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.decode(), "Approved as role : Customer Rep")

    def test_login_feature_wrong_password(self):
        body = {
            "username": "user1",
            "password": "wrong_password"
        }
        response = test_client.post('/login', json=body)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.data.decode(), "Not approved")

    def test_login_feature_wrong_username(self):
        body = {
            "username": "not_existing_user",
            "password": "wrong_password"
        }
        response = test_client.post('/login', json=body)
        self.assertEqual(response.status_code, 500)
        self.assertEqual(response.data.decode(), "Invalid username.")

    # testing customer endpoint

    def test_create_order_one_product_not_existing_model(self):
        # login as customer
        body = {
            "username": "user2",
            "password": "password2"
        }
        test_client.post('/login', json=body)

        body = {
            "model": "not_existing_model",
            "quantity": "1",
            "customer_id": "1",
            "size": "142"
        }
        response = test_client.post('/customer/create_order_one_product', json=body)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(), "Not a valid model name.")

    def test_create_order_one_product_not_valid_size(self):
        # login as customer
        body = {
            "username": "user2",
            "password": "password2"
        }
        test_client.post('/login', json=body)

        body = {
            "model": "skate",
            "quantity": "1",
            "customer_id": "1",
            "size": "1000000000000"
        }
        response = test_client.post('/customer/create_order_one_product', json=body)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data.decode(), "Not a valid size.")

    def test_create_order_one_product_valid(self):
        # login as customer
        body = {
            "username": "user2",
            "password": "password2"
        }
        test_client.post('/login', json=body)

        body = {
            "model": "skate",
            "quantity": "1",
            "customer_id": "1",
            "size": "142"
        }
        response = test_client.post('/customer/create_order_one_product', json=body)
        self.assertEqual(response.status_code, 201)

    def test_create_order_one_product_get(self):
        response = test_client.get('/customer/create_order_one_product')
        self.assertEqual(response.status_code, 405)




if __name__ == '__main__':
    unittest.main()
