# DatabaseProject
Any models made will be under the documents/models. The database as a .sql file lies under documents/sql and lastly 
the python code needed will be under documents/python. In some folders there will be a folder called old which 
contains old versions. 

## Authorization
To access any of the role specified endpoint you will need to login through the login endpoint using POST request.
Below you will find an overview over the different password and usernames for the different roles. They are unreadable 
from the database since we hashed the passwords.
The application currently doesn`t have any logout functions so if you need to change roles just login to the needed role.


```
- /login {POST}
- Storekeeper:
{
    "username": "user1",
    "password": "password1"
}

- Customer:
{
    "username": "user2",
    "password": "password2"
}

- Customer Rep:
{
    "username": "user3",
    "password": "password3"
}

- Production Planner:
{
    "username": "user4",
    "password": "password4"
}

- Transporter:
{
    "username": "user5",
    "password": "password5"
}
```


## Endpoint
### Public endpoint
Gets all skis in the product table. Filter model and size is both optional to limit search.
```
- /get_ski_types?model={model}&size={size} {GET}
- example: /get_ski_types {GET} 
- example: /get_ski_types?model=R-Skin Race {GET}
- example: /get_ski_types?model=R-Skin Race&size=172 {GET}
```

### Customer Rep endpoint
Gets all the orders stored in the productorder table. Filter state is optional. 
```
- /customer_rep/retrieve_order?state={state} {GET}
- example: /customer_rep/retrieve_order {GET}
- example: /customer_rep/retrieve_order?state=open {GET}
```
In this endpoint the user needs to add id and state in the body of the post request.
```
- /customer_rep/change_state {POST}
- example of body: 
{
    "id": "1",
    "state": "open"
}
```
This endpoint allows users logged in as customer rep to create new shipments. Note
that this endpoint requires valid id input. For the product_order_id the order also
needs to have state set as "ready to shipped".
```
- /customer_rep/create_shipment {POST}
- example of body:
{
    "product_order_id": 10,
    "name": "XXL",
    "address": "teknologivegen 22",
    "pickup_date": "2022-05-10",
    "transporter_id": 1,
    "driver_id": 1
}
```


### Storekeeper
The first endpoint for storekeeper will find any order with the state "skis available" and return them to the user.
```
- /storekeeper/retrieve_orders {GET}
```
The next endpoint for storekeeper is to change the state of a given order to "ready to be shipped".
```
- /storekeeper/fill_order {POST}
- example of body:
{
    "id": "1"
}
```
This endpoint can be used by storekeeper to updated stock for a given model. 
```
-/storekeeper/update_stock {POST}
- example of body:
{
   "model_name": "R-Skin Race,
   "ski_type": "classic",
   "size": 142,
   "quantity": 1
}
```
This endpoint is used to add new product to the database. Url is ment to be a link to an image of the skis.
```
- /storekeeper/new_product {POST}
- example of body:
{
    "model": "Redline Skate jr",
    "type": "Skate",
    "description": "easy to use ski",
    "historical": 0,
    "url": "xxl.no",
    "msrp": 2500
    "manufacturer_id": 1
}
```
### Customer
The first endpoint for customer, allows any user logged in as customer to place an order for a given model.
In this endpoint the user is only allowed to include one product of quantity and size.
```
- /customer/create_order_one_product {POST}
- example of body:
{
    "model": "R-Skin Race",
    "quantity": 10,
    "customer_id": 1,
    "size": 192
}
```
The next endpoint allows a customer to order multiple products in one request, which will be stored in one order.
The products must be a list of list. The inner list must be in the order: mode, quantity, size.
```
- /customer/create_order_multiple_products {POST}
- example of body:
{
    "customer_id": 1,
    "products": [
        ["R-Skin Race", 10, 142],
        ["REDSTER SX", 5, 162]
    ]
}
```
The next endpoint is used to allow user to cancel a given order.
```
- /customer/cancel_order {POST}
- example of body:
{
    "customer_id": 1,
    "order_id": 1
}
```
Lastly we have the endpoint used by customer to retrieve production plans. Will return all production plans stored 
in database. 
```
- /customer/retrieve_production_plan {GET}
```
### Production Planner
Production planner one has one endpoint which is used to create new production plans.
Currently, there is no option to specify date, will take start date as today's date and end date
as 4-weeks in the further. 
```
- /production_planner/create_new_production_plan {POST}
- exmaple of body:
{
    "employee_id": 4,
    "product_model": "REDSTER SX", 
    "quantity": 100
}
```
### Transporter
First endpoint for transporter is used to get all orders with state set as "ready to be shipped".
```
- /transporter/retrieve_info {GET}
```
Last endpoint for transporter is used to change the status of a shipment to "picked up" to indicate that the order 
is on the way to the customer. 
```
- /transporter/change_state {POST}
- example of body:
{
    "shipment_id": 1
}
```